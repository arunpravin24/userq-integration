// SPDX-License-Identifier: MIT
/*
 * Copyright 2023 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <linux/kref.h>
#include <linux/slab.h>

#include <drm/drm_exec.h>
#include <drm/drm_syncobj.h>

#include "amdgpu.h"
#include "amdgpu_userq_fence.h"

static const struct dma_fence_ops amdgpu_userq_fence_ops;
static struct kmem_cache *amdgpu_userq_fence_slab;

int amdgpu_userq_fence_slab_init(void)
{
	amdgpu_userq_fence_slab = kmem_cache_create("amdgpu_userq_fence",
						    sizeof(struct amdgpu_userq_fence),
						    0,
						    SLAB_HWCACHE_ALIGN,
						    NULL);
	if (!amdgpu_userq_fence_slab)
		return -ENOMEM;

	return 0;
}

void amdgpu_userq_fence_slab_fini(void)
{
	rcu_barrier();
	kmem_cache_destroy(amdgpu_userq_fence_slab);
}

static inline struct amdgpu_userq_fence *to_amdgpu_userq_fence(struct dma_fence *f)
{
	if (!f || f->ops != &amdgpu_userq_fence_ops)
		return NULL;

	return container_of(f, struct amdgpu_userq_fence, base);
}

static u64 amdgpu_userq_fence_read(struct amdgpu_userq_fence_driver *fence_drv)
{
	return le64_to_cpu(*fence_drv->cpu_addr);
}

int amdgpu_userq_fence_driver_alloc(struct amdgpu_device *adev,
				    struct amdgpu_usermode_queue *userq)
{
	struct amdgpu_userq_fence_driver *fence_drv;
	unsigned long flags;
	int r;

	fence_drv = kzalloc(sizeof(*fence_drv), GFP_KERNEL);
	if (!fence_drv) {
		DRM_ERROR("Failed to allocate memory for fence driver\n");
		return -ENOMEM;
	}

	/* Acquire seq64 memory */
	r = amdgpu_seq64_alloc(adev, &fence_drv->gpu_addr,
			       &fence_drv->cpu_addr);
	if (r) {
		kfree(fence_drv);
		return -ENOMEM;
	}

	kref_init(&fence_drv->refcount);
	INIT_LIST_HEAD(&fence_drv->fences);
	spin_lock_init(&fence_drv->fence_list_lock);

	fence_drv->adev = adev;
	fence_drv->context = dma_fence_context_alloc(1);
	get_task_comm(fence_drv->timeline_name, current);

	xa_lock_irqsave(&adev->userq_xa, flags);
	__xa_store(&adev->userq_xa, userq->doorbell_index,
		   fence_drv, GFP_KERNEL);
	xa_unlock_irqrestore(&adev->userq_xa, flags);

	userq->fence_drv = fence_drv;

	return 0;
}

void amdgpu_userq_fence_driver_process(struct amdgpu_userq_fence_driver *fence_drv)
{
	struct amdgpu_userq_fence_driver *stored_fence_drv;
	struct amdgpu_userq_fence *userq_fence, *tmp;
	unsigned long index, flags;
	struct dma_fence *fence;
	struct xarray *xa;
	u64 rptr;

	if (!fence_drv)
		return;

	rptr = amdgpu_userq_fence_read(fence_drv);

	spin_lock(&fence_drv->fence_list_lock);
	list_for_each_entry_safe(userq_fence, tmp, &fence_drv->fences, link) {
		fence = &userq_fence->base;
		xa = &userq_fence->fence_drv_xa;

		if (rptr < fence->seqno)
			break;

		dma_fence_signal(fence);
		/*
		 * Walk over the fence_drv xarray and drop the old wait ioctl
		 * fence_drv references.
		 */
		xa_lock_irqsave_nested(xa, flags, SINGLE_DEPTH_NESTING);
		xa_for_each(xa, index, stored_fence_drv) {
			__xa_erase(xa, index);
			amdgpu_userq_fence_driver_put(stored_fence_drv);
		}
		xa_unlock_irqrestore(xa, flags);

		list_del(&userq_fence->link);
		dma_fence_put(fence);
	}
	spin_unlock(&fence_drv->fence_list_lock);
}

void amdgpu_userq_fence_driver_destroy(struct kref *ref)
{
	struct amdgpu_userq_fence_driver *fence_drv = container_of(ref,
					 struct amdgpu_userq_fence_driver,
					 refcount);
	struct amdgpu_device *adev = fence_drv->adev;
	struct amdgpu_userq_fence *fence, *tmp;
	struct dma_fence *f;

	spin_lock(&fence_drv->fence_list_lock);
	list_for_each_entry_safe(fence, tmp, &fence_drv->fences, link) {
		f = &fence->base;

		if (!dma_fence_is_signaled(f)) {
			dma_fence_set_error(f, -ECANCELED);
			dma_fence_signal(f);
		}

		list_del(&fence->link);
		dma_fence_put(f);
	}
	spin_unlock(&fence_drv->fence_list_lock);

	/* Free seq64 memory */
	amdgpu_seq64_free(adev, fence_drv->gpu_addr);
	kfree(fence_drv);
}

void amdgpu_userq_fence_driver_get(struct amdgpu_userq_fence_driver *fence_drv)
{
	kref_get(&fence_drv->refcount);
}

void amdgpu_userq_fence_driver_put(struct amdgpu_userq_fence_driver *fence_drv)
{
	kref_put(&fence_drv->refcount, amdgpu_userq_fence_driver_destroy);
}

int amdgpu_userq_fence_create(struct amdgpu_usermode_queue *userq,
			      u64 seq, struct dma_fence **f)
{
	struct amdgpu_userq_fence_driver *fence_drv;
	struct amdgpu_userq_fence *userq_fence;
	struct dma_fence *fence;

	fence_drv = userq->fence_drv;
	if (!fence_drv)
		return -EINVAL;

	userq_fence = kmem_cache_alloc(amdgpu_userq_fence_slab, GFP_ATOMIC);
	if (!userq_fence)
		return -ENOMEM;

	spin_lock_init(&userq_fence->lock);
	INIT_LIST_HEAD(&userq_fence->link);
	fence = &userq_fence->base;
	userq_fence->fence_drv = fence_drv;

	dma_fence_init(fence, &amdgpu_userq_fence_ops, &userq_fence->lock,
		       fence_drv->context, seq);

	xa_init_flags(&userq_fence->fence_drv_xa, XA_FLAGS_LOCK_IRQ);

	amdgpu_userq_fence_driver_get(fence_drv);
	dma_fence_get(fence);

	if (!xa_empty(&userq->uq_fence_drv_xa)) {
		struct amdgpu_userq_fence_driver *stored_fence_drv;
		unsigned long flags, index;

		/*
		 * Move fence_drv references of old signal IOCTL calls to the
		 * newly created userq fence xarray.
		 */
		xa_lock(&userq->uq_fence_drv_xa);
		xa_for_each(&userq->uq_fence_drv_xa, index, stored_fence_drv) {
			/* Skip if the queues are different */
			if (stored_fence_drv != fence_drv)
				continue;

			xa_lock_irqsave_nested(&userq_fence->fence_drv_xa, flags,
					       SINGLE_DEPTH_NESTING);
			__xa_store(&userq_fence->fence_drv_xa, index,
				   stored_fence_drv, GFP_KERNEL);
			xa_unlock_irqrestore(&userq_fence->fence_drv_xa, flags);

			/* Erase fence_drv reference entry from userq xarray */
			__xa_erase(&userq->uq_fence_drv_xa, index);
		}
		xa_unlock(&userq->uq_fence_drv_xa);
	}

	spin_lock(&fence_drv->fence_list_lock);
	/* Check if hardware has already processed the job */
	if (!dma_fence_is_signaled(fence))
		list_add_tail(&userq_fence->link, &fence_drv->fences);
	else
		dma_fence_put(fence);

	spin_unlock(&fence_drv->fence_list_lock);

	*f = fence;

	return 0;
}

static const char *amdgpu_userq_fence_get_driver_name(struct dma_fence *f)
{
	return "amdgpu_userqueue_fence";
}

static const char *amdgpu_userq_fence_get_timeline_name(struct dma_fence *f)
{
	struct amdgpu_userq_fence *fence = to_amdgpu_userq_fence(f);

	return fence->fence_drv->timeline_name;
}

static bool amdgpu_userq_fence_signaled(struct dma_fence *f)
{
	struct amdgpu_userq_fence *fence = to_amdgpu_userq_fence(f);
	struct amdgpu_userq_fence_driver *fence_drv = fence->fence_drv;
	u64 rptr, wptr;

	rptr = amdgpu_userq_fence_read(fence_drv);
	wptr = fence->base.seqno;

	if (rptr >= wptr)
		return true;

	return false;
}

static void amdgpu_userq_fence_free(struct rcu_head *rcu)
{
	struct dma_fence *fence = container_of(rcu, struct dma_fence, rcu);
	struct amdgpu_userq_fence *userq_fence = to_amdgpu_userq_fence(fence);
	struct amdgpu_userq_fence_driver *fence_drv = userq_fence->fence_drv;

	/* Release the fence driver reference */
	amdgpu_userq_fence_driver_put(fence_drv);

	xa_destroy(&userq_fence->fence_drv_xa);
	kmem_cache_free(amdgpu_userq_fence_slab, userq_fence);
}

static void amdgpu_userq_fence_release(struct dma_fence *f)
{
	call_rcu(&f->rcu, amdgpu_userq_fence_free);
}

static const struct dma_fence_ops amdgpu_userq_fence_ops = {
	.use_64bit_seqno = true,
	.get_driver_name = amdgpu_userq_fence_get_driver_name,
	.get_timeline_name = amdgpu_userq_fence_get_timeline_name,
	.signaled = amdgpu_userq_fence_signaled,
	.release = amdgpu_userq_fence_release,
};

/**
 * amdgpu_userq_fence_read_wptr - Read the userq wptr value
 *
 * @filp: drm file private data structure
 * @queue: user mode queue structure pointer
 * @wptr: write pointer value
 *
 * Read the wptr value from userq's MQD. The userq signal IOCTL
 * creates a dma_fence for the shared buffers that expects the
 * RPTR value written to seq64 memory >= WPTR.
 *
 * Returns wptr value on success, error on failure.
 */
static int amdgpu_userq_fence_read_wptr(struct drm_file *filp,
					struct amdgpu_usermode_queue *queue,
					u64 *wptr)
{
	struct amdgpu_fpriv *fpriv = filp->driver_priv;
	struct amdgpu_bo_va_mapping *mapping;
	struct amdgpu_vm *vm = &fpriv->vm;
	struct amdgpu_bo *bo;
	u64 addr, *ptr;
	int r;

	addr = queue->userq_prop->wptr_gpu_addr >> PAGE_SHIFT;

	mapping = amdgpu_vm_bo_lookup_mapping(vm, addr);
	if (!mapping)
		return -EINVAL;

	bo = mapping->bo_va->base.bo;
	r = amdgpu_bo_reserve(bo, true);
	if (r) {
		DRM_ERROR("Failed to reserve userqueue wptr bo");
		return r;
	}

	r = amdgpu_bo_kmap(bo, (void **)&ptr);
	if (r) {
		DRM_ERROR("Failed mapping the userqueue wptr bo");
		goto map_error;
	}

	*wptr = le64_to_cpu(*ptr);

	amdgpu_bo_kunmap(bo);
	amdgpu_bo_unreserve(bo);

	return 0;

map_error:
	amdgpu_bo_unreserve(bo);
	return r;
}

int amdgpu_userq_signal_ioctl(struct drm_device *dev, void *data,
			      struct drm_file *filp)
{
	struct amdgpu_fpriv *fpriv = filp->driver_priv;
	struct amdgpu_userq_mgr *userq_mgr = &fpriv->userq_mgr;
	struct drm_amdgpu_userq_signal *args = data;
	struct amdgpu_usermode_queue *queue;
	struct drm_gem_object **gobj = NULL;
	struct drm_syncobj *syncobj = NULL;
	u32 *bo_handles, num_bo_handles;
	struct dma_fence *fence;
	struct drm_exec exec;
	int r, i, entry;
	u64 wptr;

	/* Array of bo handles */
	num_bo_handles = args->num_bo_handles;
	bo_handles = memdup_user(u64_to_user_ptr(args->bo_handles_array),
				 sizeof(u32) * num_bo_handles);
	if (IS_ERR(bo_handles))
		return PTR_ERR(bo_handles);

	/* Array of GEM object handles */
	gobj = kmalloc_array(num_bo_handles, sizeof(*gobj), GFP_KERNEL);
	if (!gobj) {
		r = -ENOMEM;
		goto free_bo_handles;
	}

	for (entry = 0; entry < num_bo_handles; entry++) {
		gobj[entry] = drm_gem_object_lookup(filp, bo_handles[entry]);
		if (!gobj[entry]) {
			r = -ENOENT;
			goto put_gobj;
		}
	}

	/* Find Syncobj if any */
	syncobj = drm_syncobj_find(filp, args->syncobj_handle);
	if (args->syncobj_handle && !syncobj) {
		r = -ENOENT;
		goto put_gobj;
	}

	drm_exec_init(&exec, DRM_EXEC_INTERRUPTIBLE_WAIT);
	drm_exec_until_all_locked(&exec) {
		r = drm_exec_prepare_array(&exec, gobj, num_bo_handles, 1);
		drm_exec_retry_on_contention(&exec);
		if (r)
			goto exec_fini;
	}

	/*Retrieve the user queue */
	queue = idr_find(&userq_mgr->userq_idr, args->queue_id);
	if (!queue) {
		r = -ENOENT;
		goto exec_fini;
	}

	r = amdgpu_userq_fence_read_wptr(filp, queue, &wptr);
	if (r)
		goto exec_fini;

	/* Create a new fence */
	r = amdgpu_userq_fence_create(queue, wptr, &fence);
	if (r)
		goto exec_fini;

	for (i = 0; i < num_bo_handles; i++)
		dma_resv_add_fence(gobj[i]->resv, fence,
				   dma_resv_usage_rw(args->bo_flags &
				   AMDGPU_USERQ_BO_WRITE));

	/* Add the created fence to syncobj/BO's */
	if (syncobj)
		drm_syncobj_replace_fence(syncobj, fence);

	/* drop the reference acquired in fence creation function */
	dma_fence_put(fence);

exec_fini:
	drm_exec_fini(&exec);
put_gobj:
	while (entry-- > 0)
		drm_gem_object_put(gobj[entry]);
	kfree(gobj);
free_bo_handles:
	kfree(bo_handles);

	return r;
}

int amdgpu_userq_wait_ioctl(struct drm_device *dev, void *data,
			    struct drm_file *filp)
{
	struct amdgpu_fpriv *fpriv = filp->driver_priv;
	struct amdgpu_userq_mgr *userq_mgr = &fpriv->userq_mgr;
	struct drm_amdgpu_userq_fence_info *fence_info = NULL;
	struct drm_amdgpu_userq_wait *wait_info = data;
	struct amdgpu_usermode_queue *waitq;
	u32 *syncobj_handles, *bo_handles;
	struct dma_fence **fences = NULL;
	u32 num_syncobj, num_bo_handles;
	struct drm_gem_object **gobj;
	struct drm_exec exec;
	int r, i, entry, cnt;
	u64 num_fences = 0;

	num_bo_handles = wait_info->num_bo_handles;
	bo_handles = memdup_user(u64_to_user_ptr(wait_info->bo_handles_array),
				 sizeof(u32) * num_bo_handles);
	if (IS_ERR(bo_handles))
		return PTR_ERR(bo_handles);

	num_syncobj = wait_info->num_syncobj_handles;
	syncobj_handles = memdup_user(u64_to_user_ptr(wait_info->syncobj_handles_array),
				      sizeof(u32) * num_syncobj);
	if (IS_ERR(syncobj_handles)) {
		r = PTR_ERR(syncobj_handles);
		goto free_bo_handles;
	}

	/* Array of GEM object handles */
	gobj = kmalloc_array(num_bo_handles, sizeof(*gobj), GFP_KERNEL);
	if (!gobj) {
		r = -ENOMEM;
		goto free_syncobj_handles;
	}

	for (entry = 0; entry < num_bo_handles; entry++) {
		gobj[entry] = drm_gem_object_lookup(filp, bo_handles[entry]);
		if (!gobj[entry]) {
			r = -ENOENT;
			goto put_gobj;
		}
	}

	drm_exec_init(&exec, DRM_EXEC_INTERRUPTIBLE_WAIT);
	drm_exec_until_all_locked(&exec) {
		r = drm_exec_prepare_array(&exec, gobj, num_bo_handles, 0);
		drm_exec_retry_on_contention(&exec);
		if (r) {
			drm_exec_fini(&exec);
			goto put_gobj;
		}
	}

	if (!wait_info->num_fences) {
		/* Count syncobj's fence */
		for (i = 0; i < num_syncobj; i++) {
			struct dma_fence *fence;

			r = drm_syncobj_find_fence(filp, syncobj_handles[i],
						   0, 0, &fence);
			dma_fence_put(fence);

			if (r || !fence)
				continue;

			num_fences++;
		}

		/* Count GEM objects fence */
		for (i = 0; i < num_bo_handles; i++) {
			struct dma_resv_iter resv_cursor;
			struct dma_fence *fence;

			dma_resv_for_each_fence(&resv_cursor, gobj[i]->resv,
						dma_resv_usage_rw(wait_info->bo_wait_flags &
						AMDGPU_USERQ_BO_WRITE), fence)
				num_fences++;
		}

		/*
		 * Passing num_fences = 0 means that userspace doesn't want to
		 * retrieve userq_fence_info. If num_fences = 0 we skip filling
		 * userq_fence_info and return the actual number of fences on
		 * args->num_fences.
		 */
		wait_info->num_fences = num_fences;
	} else {
		/* Array of fence info */
		fence_info = kmalloc_array(wait_info->num_fences, sizeof(*fence_info), GFP_KERNEL);
		if (!fence_info) {
			r = -ENOMEM;
			goto exec_fini;
		}

		/* Array of fences */
		fences = kmalloc_array(wait_info->num_fences, sizeof(*fences), GFP_KERNEL);
		if (!fences) {
			r = -ENOMEM;
			goto free_fence_info;
		}

		/* Retrieve GEM objects fence */
		for (i = 0; i < num_bo_handles; i++) {
			struct dma_resv_iter resv_cursor;
			struct dma_fence *fence;

			dma_resv_for_each_fence(&resv_cursor, gobj[i]->resv,
						dma_resv_usage_rw(wait_info->bo_wait_flags &
						AMDGPU_USERQ_BO_WRITE), fence) {
				if (WARN_ON_ONCE(num_fences >= wait_info->num_fences)) {
					r = -EINVAL;
					goto free_fences;
				}

				fences[num_fences++] = fence;
				dma_fence_get(fence);
			}
		}

		/* Retrieve syncobj's fence */
		for (i = 0; i < num_syncobj; i++) {
			struct dma_fence *fence;

			r = drm_syncobj_find_fence(filp, syncobj_handles[i],
						   0, 0, &fence);
			if (r || !fence)
				continue;

			if (WARN_ON_ONCE(num_fences >= wait_info->num_fences)) {
				r = -EINVAL;
				goto free_fences;
			}

			fences[num_fences++] = fence;
		}

		waitq = idr_find(&userq_mgr->userq_idr, wait_info->waitq_id);
		if (!waitq)
			goto free_fences;

		for (i = 0, cnt = 0; i < wait_info->num_fences; i++) {
			struct amdgpu_userq_fence_driver *fence_drv;
			struct amdgpu_userq_fence *userq_fence;
			u32 index;

			userq_fence = to_amdgpu_userq_fence(fences[i]);
			if (!userq_fence) {
				/*
				 * Just waiting on other driver fences should
				 * be good for now
				 */
				dma_fence_wait(fences[i], false);
				dma_fence_put(fences[i]);

				continue;
			}

			fence_drv = userq_fence->fence_drv;
			/*
			 * We need to make sure the user queue release their reference
			 * to the fence drivers at some point before queue destruction.
			 * Otherwise, we would gather those references until we don't
			 * have any more space left and crash.
			 */
			r = xa_alloc(&waitq->uq_fence_drv_xa, &index,
				     fence_drv, xa_limit_32b, GFP_KERNEL);
			if (r)
				goto free_fences;

			amdgpu_userq_fence_driver_get(fence_drv);

			/* Store drm syncobj's gpu va address and value */
			fence_info[cnt].va = fence_drv->gpu_addr;
			fence_info[cnt].value = fences[i]->seqno;

			dma_fence_put(fences[i]);
			/* Increment the actual userq fence count */
			cnt++;
		}

		wait_info->num_fences = cnt;
		/* Copy userq fence info to user space */
		if (copy_to_user(u64_to_user_ptr(wait_info->userq_fence_info),
				 fence_info, wait_info->num_fences * sizeof(*fence_info))) {
			r = -EFAULT;
			goto free_fences;
		}

		kfree(fences);
		kfree(fence_info);
	}

	drm_exec_fini(&exec);
	for (i = 0; i < num_bo_handles; i++)
		drm_gem_object_put(gobj[i]);

	kfree(syncobj_handles);
	kfree(bo_handles);

	return 0;

free_fences:
	kfree(fences);
free_fence_info:
	kfree(fence_info);
exec_fini:
	drm_exec_fini(&exec);
put_gobj:
	while (entry-- > 0)
		drm_gem_object_put(gobj[entry]);
	kfree(gobj);
free_syncobj_handles:
	kfree(syncobj_handles);
free_bo_handles:
	kfree(bo_handles);

	return r;
}
